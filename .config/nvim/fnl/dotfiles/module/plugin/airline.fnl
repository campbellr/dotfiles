(module dotfiles.module.plugin.airline
  {require {nvim aniseed.nvim}})

;; Use gruvbox airline theme
(set nvim.g.airline_theme :gruvbox)
