(module dotfiles.module.plugin.vim-gitgutter
  {require {nvim aniseed.nvim}})

(set nvim.g.gitgutter_highlight_linenrs true)
